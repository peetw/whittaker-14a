# Utilities
LATEXMK = latexmk
GNUPLOT = gnuplot

# Main TeX and PDF files
TEX_SRC := $(shell find . -type f -name "*.tex")
MAIN_SRC = whittaker-14a.tex
MAIN_PDF := $(MAIN_SRC:.tex=.pdf)

# BibTeX files
BIB_SRC := $(MAIN_SRC:.tex=.bib) elsarticle-num.bst

# Gnuplot files
IMG_DIR = images
GNU_SRC := $(shell find . -type f -name "*.plt" -o -name "*.gnu")
ifdef GNU_SRC
GNU_OUT := $(shell grep -H -e "set out" -e "RESULTS =" $(GNU_SRC) | sed 's/\(.*\/\).*\.[pltgnu]*.*"\(.*\)"/\1\2/')
endif
GNU_PDF := $(filter %.pdf,$(GNU_OUT))
GNU_DEP = .depend

# Submission directory and files
SUB_DIR = submission
SUB_TAR = $(SUB_DIR)/LaTeX.tar.gz
SUB_SRC := $(TEX_SRC) $(BIB_SRC) $(GNU_PDF) elsarticle.cls

# Ensure make runs in serial (parallel build breaks gnuplot dependencies)
.NOTPARALLEL:

# Non-file targets
.PHONY: all force submission clean clean-pdf clean-extra clean-all

# Default target
all: $(MAIN_PDF)

# Force compilation of main PDF
force:
	$(LATEXMK) -f -g -pdf -pdflatex="pdflatex --shell-escape %O %S" $(MAIN_SRC)

# Compile main PDF
$(MAIN_PDF): $(TEX_SRC) $(BIB_SRC) $(GNU_PDF)
	$(LATEXMK) -f -pdf -pdflatex="pdflatex --shell-escape %O %S" $(MAIN_SRC)

# Determine gnuplot dependencies
$(GNU_DEP): $(GNU_SRC)
	./makegnudep $^ > $(GNU_DEP)

-include $(GNU_DEP)

# Generate gnuplot files
$(GNU_OUT):
	@mkdir -p $(IMG_DIR)
	cd $(shell dirname $<) && $(GNUPLOT) $(shell basename $<) 2>/dev/null

# Organize files ready for submission to journal
submission: $(SUB_TAR)

$(SUB_TAR): $(SUB_SRC) $(MAIN_PDF)
	@mkdir -p $(SUB_DIR)
	cp $(SUB_SRC) $(SUB_DIR)
	cd $(SUB_DIR) && tar -czf ../$@ *.* --remove-files
	cp $(MAIN_PDF) $(SUB_DIR)

# Compile PS file for submission
$(SUB_PS): $(TEX_SRC) $(GNU_PDF)
	$(LATEXMK) -f -silent -ps $(MAIN_SRC)

# Remove intermediary LaTeX files
clean: clean-extra
	$(LATEXMK) -silent -c $(MAIN_SRC)

# Remove all intermediary LaTeX files, including PDF, DVI and PS
clean-pdf: clean-extra
	$(LATEXMK) -silent -C $(MAIN_SRC)

# Remove intermediary LaTeX files that latexmk misses
clean-extra:
	$(RM) $(MAIN_SRC:.tex=.bbl)
	$(RM) $(MAIN_SRC:.tex=.fff)
	$(RM) $(MAIN_SRC:.tex=.spl)
	$(RM) $(MAIN_SRC:.tex=.ttt)

# Remove all generated files
clean-all: clean-pdf
	$(RM) $(GNU_OUT)
	$(RM) $(GNU_DEP)
	$(RM) -r $(SUB_DIR)
