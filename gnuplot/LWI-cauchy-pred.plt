#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
DATA = "../data/~LWI-cauchy-pred.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.3 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "Measured, {/NimbusRomNo9L-ReguItal F} (N)" offset 0,0.5
set ylabel "Predicted, {/NimbusRomNo9L-ReguItal F} (N)" offset 2.5,0
set xrange [0.1:10]
set yrange [0.1:10]
set xtics scale 0.5 font ",16"
set ytics scale 0.5 font ",16"
set logscale xy
set style data points
set pointsize 0.6
set key left Left reverse box lw 0.5 height 0.3 width 1.5 samplen 3 font ",16"

set output "../images/fig-3-LWI-cauchy-pred.pdf"
set multiplot layout 1,2

# Functions
f(x) = x


###############################################################################
# PARTIALLY SUBMERGED
###############################################################################

set label 1 "(a)" at graph 0.91,0.09 center font ",18"

plot	DATA index 0 u 1:2 pt 1 t "WN", \
		DATA index 2 u 1:2 pt 4 t "PN", \
		DATA index 4 u 1:2 pt 6 t "PA", \
		f(x) w lines lt 2 t "1:1"


###############################################################################
# FULLY SUBMERGED
###############################################################################

set label 1 "(b)"

plot	DATA index 1 u 1:2 pt 1 t "WN", \
		DATA index 3 u 1:2 pt 4 t "PN", \
		DATA index 5 u 1:2 pt 6 t "PA", \
		f(x) w lines lt 2 t "1:1"
