#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
FOL_DATA = "../data/~cauchy-pred-EI25-fol.csv"
DEFOL_DATA = "../data/~cauchy-pred-EI25-defol.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.3 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "Measured, {/NimbusRomNo9L-ReguItal F} (N)" offset 0,0.5
set ylabel "Predicted, {/NimbusRomNo9L-ReguItal F} (N)" offset 2.5,0
set xrange [1:1000]
set yrange [1:1000]
set xtics scale 0.5 font ",16"
set ytics scale 0.5 font ",16"
set logscale xy
set style data points
set pointsize 0.6
set key left Left reverse box lw 0.5 height 0.3 width -1 samplen 3 font ",16"

set output "../images/fig-2-cauchy-pred.pdf"
set multiplot layout 1,2

# Functions
f(x) = x


###############################################################################
# FOLIATED
###############################################################################

set label 1 "(a)" at graph 0.91,0.09 center font ",18"

plot	FOL_DATA index 0:4 u 1:2 pt 1 t "{/NimbusRomNo9L-ReguItal A. glutinosa}", \
		FOL_DATA index 5:7 u 1:2 pt 4 t "{/NimbusRomNo9L-ReguItal P. nigra}", \
		FOL_DATA index 8::1 u 1:2 pt 6 t "{/NimbusRomNo9L-ReguItal S. alba}", \
		f(x) w lines lt 2 t "1:1"


###############################################################################
# DEFOLIATED
###############################################################################

set label 1 "(b)"
set key width 1

plot	DEFOL_DATA index 0 u 1:2 pt 1 t "A1", \
		DEFOL_DATA index 1 u 1:2 pt 4 t "P1", \
		DEFOL_DATA index 2 u 1:2 pt 6 t "S2", \
		f(x) w lines lt 2 t "1:1"
