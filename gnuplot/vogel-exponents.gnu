#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Set filenames and variables
FU_DATA = "../data/F-vs-U.csv"
RESULTS = "../data/~vogel-exponents.csv"

# CSV input
set datafile separator comma

# Create results file
set macro
!echo "#,Foliated,,,,Defoliated" > @RESULTS
!echo "#Specimen,Umin,Umax,b,Rsq,Umin,Umax,b,Rsq" >> @RESULTS

# Functions for data fitting
f(x) = K * x**(2+b)
g(x) = mean

# Loop through all F-vs-U data to perform linear regression
do for [i=1:156:5] {

	# Get specimen name
	SPECIMEN = system(sprintf("awk -F, 'NR == 1 {print $%i}' %s", i, FU_DATA))

	###########################################################################
	# FOLIATED
	###########################################################################

	# Ignore data sets with no valid points
	if (SPECIMEN eq "P1") {
		# No data
		Umin_fol = "NULL"; Umax_fol = "NULL"; b_fol = "NULL"; RSQ_fol = "NULL"
	} else {
		# Get minimum and maximum velocities
		stats FU_DATA u i:(column(i+1)) nooutput
		Umin_fol = sprintf("%.3f", STATS_pos_min_y)
		Umax_fol = sprintf("%.3f", STATS_pos_max_y)

		# Fit power-law function to data
		K = 100
		b = -0.75
		fit f(x) FU_DATA u i:(column(i+1)) via K, b

		# Retrieve parameters and residual sum of squares
		K_fol = K
		b_fol = sprintf("%.4f", b)
		SSR = FIT_WSSR

		# Determine total sum of squares
		mean = 10
		fit g(x) FU_DATA u i:(column(i+1)) via mean
		SST = FIT_WSSR

		# Calculate R2
		RSQ_fol = sprintf("%.4f", 1 - SSR/SST)
	}


	###########################################################################
	# DEFOLIATED
	###########################################################################

	# Ignore data sets with no valid points
	if (SPECIMEN eq "A2" || \
		SPECIMEN eq "P2" || SPECIMEN eq "P4" || \
		SPECIMEN eq "S5" || SPECIMEN eq "S6" || SPECIMEN eq "S7") {
		# No data
		Umin_defol = "NULL"; Umax_defol = "NULL"; b_defol = "NULL"; RSQ_defol = "NULL"
	} else {
		# Get minimum and maximum velocities
		stats FU_DATA u i:(column(i+2)) nooutput
		Umin_defol = sprintf("%.3f", STATS_pos_min_y)
		Umax_defol = sprintf("%.3f", STATS_pos_max_y)

		# Fit power-law function to data
		K = 100
		b = -0.75
		fit f(x) FU_DATA u i:(column(i+2)) via K, b

		# Retrieve parameters and residual sum of squares
		K_defol = K
		b_defol = sprintf("%.4f", b)
		SSR = FIT_WSSR

		# Determine total sum of squares
		mean = 10
		fit g(x) FU_DATA u i:(column(i+2)) via mean
		SST = FIT_WSSR

		# Calculate R2
		RSQ_defol = sprintf("%.4f", 1 - SSR/SST)
	}


	###########################################################################
	# OUTPUT
	###########################################################################

	# Add spacing between species
	if (SPECIMEN eq "P1" || SPECIMEN eq "S1") {
		system(sprintf('echo "\n" >> %s', RESULTS))
	}

	# Output results to file
	system(sprintf('echo "%s,%s,%s,%s,%s,%s,%s,%s,%s" >> %s', SPECIMEN, Umin_fol, Umax_fol, b_fol, RSQ_fol, Umin_defol, Umax_defol, b_defol, RSQ_defol, RESULTS))
}

# Remove fitting log
!rm fit.log
