#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
OLD_FOL_DATA = "../data/~old-cauchy-reconf-EI25-fol.csv"
OLD_DEFOL_DATA = "../data/~old-cauchy-reconf-EI25-defol.csv"
FOL_DATA = "../data/~cauchy-reconf-EI25-fol.csv"
DEFOL_DATA = "../data/~cauchy-reconf-EI25-defol.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.3 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal Ca} (-)" offset 0,0.3
set ylabel "{/rtxmi ℛ} (-)" offset 0.5,0
set xrange [1E-3:1E3]
set yrange [1E-2:2]
set xtics scale 0.5 font ",16"
set ytics scale 0.5 font ",16"
set logscale xy
set format xy "10^{%L}"
set style data points
set pointsize 0.6
set key bottom left Left reverse box lw 0.5 height 0.3 width -1 samplen 3 font ",16" noautotitle
set bmargin 2.75

set output "../images/fig-1-cauchy-reconf.pdf"
set multiplot layout 1,2


###############################################################################
# FOLIATED
###############################################################################

set label 1 "(a)" at graph 0.91,0.09 center

set arrow 2 from graph 0.15,0.5 to graph 0.2,0.6 filled
set label 2 "Eq. (5)" at graph 0.15,0.45 center

set arrow 3 from graph 0.85,0.85 to graph 0.775,0.75 filled
set label 3 "Eq. (7)" at graph 0.85,0.9 center

plot	OLD_FOL_DATA index 0:4 u 1:2 pt 1 t "{/NimbusRomNo9L-ReguItal A. glutinosa}", \
		OLD_FOL_DATA index 5:7 u 1:2 pt 4 t "{/NimbusRomNo9L-ReguItal P. nigra}", \
		OLD_FOL_DATA index 8::1 u 1:2 pt 6 t "{/NimbusRomNo9L-ReguItal S. alba}", \
		FOL_DATA index 0:4 u 1:2 pt 1, \
		FOL_DATA index 5:7 u 1:2 pt 4, \
		FOL_DATA index 8::1 u 1:2 pt 6, \


###############################################################################
# DEFOLIATED
###############################################################################

set label 1 "(b)"

set arrow 2 from graph 0.175,0.575 to graph 0.225,0.675 filled
set label 2 "Eq. (5)" at graph 0.175,0.525 center

set key width 1.5

plot	OLD_DEFOL_DATA index 0 u 1:2 pt 1 t "A1", \
		OLD_DEFOL_DATA index 1 u 1:2 pt 4 t "P1", \
		OLD_DEFOL_DATA index 2 u 1:2 pt 6 t "S2", \
		DEFOL_DATA index 0 u 1:2 pt 1, \
		DEFOL_DATA index 1 u 1:2 pt 4, \
		DEFOL_DATA index 2 u 1:2 pt 6
