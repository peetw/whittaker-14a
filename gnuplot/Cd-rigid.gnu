#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Set filenames and variables
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
RESULTS = "../data/~Cd-rigid.csv"

# CSV input
set datafile separator comma

# Create results file
set macro
!echo "#,Foliated,Defoliated" > @RESULTS
!echo "#Specimen,Cd0,Cd0" >> @RESULTS

# Constants
RHO = 1000.0

# Loop through all specimens and calculate quasi-stff drag coefficients
do for [i=1:156:5] {

	# Get specimen name
	SPECIMEN = system(sprintf("awk -F, 'NR == 1 {print $%i}' %s", i, FU_DATA))

	###########################################################################
	# FOLIATED
	###########################################################################

	# Ignore data sets with no valid points
	if (SPECIMEN eq "P1") {
		# No data
		CD_FOL = "NULL"
	} else {
		# Get initial force-velocity data
		stats FU_DATA u i:(column(i+1)) nooutput
		F = STATS_min_y
		U = STATS_pos_min_y

		# Get projected area
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

		# Calculate rigid drag coefficient
		CD_FOL = sprintf("%.4f", 2.0 * F / (RHO * AP * U**2))
	}


	###########################################################################
	# DEFOLIATED
	###########################################################################

	# Ignore data sets with no valid points
	if (SPECIMEN eq "A2" || \
		SPECIMEN eq "P2" || SPECIMEN eq "P4" || \
		SPECIMEN eq "S5" || SPECIMEN eq "S6" || SPECIMEN eq "S7") {
		# No data
		CD_DEFOL = "NULL"
	} else {
		# Get minimum and maximum velocities
		stats FU_DATA u i:(column(i+2)) nooutput
		F = STATS_min_y
		U = STATS_pos_min_y

		# Get projected area
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

		# Calculate rigid drag coefficient
		CD_DEFOL = sprintf("%.4f", 2.0 * F / (RHO * AP * U**2))
	}


	###########################################################################
	# OUTPUT
	###########################################################################

	# Add spacing between species
	if (SPECIMEN eq "P1" || SPECIMEN eq "S1") {
		system(sprintf('echo "\n" >> %s', RESULTS))
	}

	# Output results to file
	system(sprintf("echo \"%s,%s,%s\" | sed 's/nan/NULL/g' >> %s", SPECIMEN, CD_FOL, CD_DEFOL, RESULTS))
}
