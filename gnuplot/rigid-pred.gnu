#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
CD_DATA = "../data/~Cd-rigid.csv"

TMP = "tmp.dat"
HEADER = "# F_meas (N),F_pred (N),err (%)"

FOL_RESULTS = "../data/~rigid-pred-fol.csv"
DEFOL_RESULTS = "../data/~rigid-pred-defol.csv"

# CSV input
set datafile separator comma

# Functions for data fitting
RHO = 1000.0

F(U) = 0.5 * RHO * CD * AP * U**2

error(meas,pred) = 100.0 * abs(meas - pred) / meas


###############################################################################
# FOLIATED
###############################################################################

# Species-averaged drag coefficients
stats CD_DATA index 0 u 2 name "ALNUS_CD" nooutput
stats CD_DATA index 1 u 2 name "POPULUS_CD" nooutput
stats CD_DATA index 2 u 2 name "SALIX_CD" nooutput

# Calculate model error
set table FOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "A2" || S eq "A3" || S eq "A4" || S eq "A5" || \
		S eq "P2" || S eq "P3" || S eq "P4" || \
		S eq "S1" || S eq "S3" || S eq "S4" || S eq "S5" || S eq "S6" || S eq "S7" || S eq "S8" || S eq "S9" || S eq "S10" || S eq "S11" || S eq "S12") {
		# Get physical properties
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3; exit}' %s", S, PH_DATA)))

		# Get rigid drag coefficient
		CD = (substr(S,1,1) eq "A" ? ALNUS_CD_mean : (substr(S,1,1) eq "P" ? POPULUS_CD_mean : SALIX_CD_mean))

		# Calculate model error
		splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i)))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, FOL_RESULTS, TMP, TMP, FOL_RESULTS))


###############################################################################
# DEFOLIATED
###############################################################################

# Species-averaged drag coefficients
stats CD_DATA index 0 u 3 name "ALNUS_CD" nooutput
stats CD_DATA index 1 u 3 name "POPULUS_CD" nooutput
stats CD_DATA index 2 u 3 name "SALIX_CD" nooutput

# Calculate model error
set table DEFOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "P1" || S eq "S1" || S eq "S2") {
		# Get physical properties
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4; exit}' %s", S, PH_DATA)))

		# Get rigid drag coefficient
		CD = (substr(S,1,1) eq "A" ? ALNUS_CD_mean : (substr(S,1,1) eq "P" ? POPULUS_CD_mean : SALIX_CD_mean))

		# Calculate model error
		splot FU_DATA u i+2:(F(column(i))):(error(column(i+2),F(column(i)))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, DEFOL_RESULTS, TMP, TMP, DEFOL_RESULTS))
