#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
FU_DATA = "../data/LWI-F-vs-U.csv"
PH_DATA = "../data/LWI-physical-properties.csv"
VG_DATA = "../data/~vogel-exponents.csv"
CD_DATA = "../data/~Cd-rigid.csv"
RESULTS = "../data/~LWI-cauchy-pred.csv"

TMP = "tmp.dat"
HEADER = "# F_meas (N),F_pred (N),err (%)"

# CSV input
set datafile separator comma

# Functions for data fitting
RHO = 1000
max(x,y) = (x > y ? x : y)

Ca(U) = max(1, (RHO * U**2 * AP * H**2) / EI)

F(U) = 0.5 * RHO * CD * AP * Ca(U)**(E/2) * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas


###############################################################################
# CAUCHY MODEL
###############################################################################

# Species-averaged foliated Vogel exponents
stats VG_DATA index 1 u 4 name "POPULUS_VOGEL" nooutput
stats VG_DATA index 2 u 4 name "SALIX_VOGEL" nooutput

# Species-averaged foliated drag coefficients
stats CD_DATA index 1 u 2 name "POPULUS_CD" nooutput
stats CD_DATA index 2 u 2 name "SALIX_CD" nooutput

# Calculate model error
set table RESULTS
do for [i = 1:11:2] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Get physical properties
	H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3; exit}' %s", S, PH_DATA)))
	EI = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5; exit}' %s", S, PH_DATA)))

	# Get Vogel exponent and rigid drag coefficient
	E = (substr(S,1,1) eq "P" ? POPULUS_VOGEL_mean : SALIX_VOGEL_mean)
	CD = (substr(S,1,1) eq "P" ? POPULUS_CD_mean : SALIX_CD_mean)

	# Calculate model error
	splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i)))) t S
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, RESULTS, TMP, TMP, RESULTS))
