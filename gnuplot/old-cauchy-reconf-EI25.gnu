#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Filenames and variables
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
CD_DATA = "../data/~Cd-rigid.csv"

TMP = "tmp.dat"
HEADER = "# Ca (-),R (-)"

FOL_RESULTS = "../data/~old-cauchy-reconf-EI25-fol.csv"
DEFOL_RESULTS = "../data/~old-cauchy-reconf-EI25-defol.csv"

# CSV input
set datafile separator comma

# Functions for data fitting
RHO = 1000
Ca(U) = RHO * U**2 * V * H / EI_25
R(U,F_meas) = F_meas / (0.5 * RHO * CD * AP * U**2)


###############################################################################
# FOLIATED
###############################################################################

# Calculate values
set table FOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "A2" || S eq "A4" || S eq "A5" || \
		S eq "P3" || \
		S eq "S3" || S eq "S10" || S eq "S11" || S eq "S12") {
		# Get physical properties
		H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3; exit}' %s", S, PH_DATA)))
		V = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))
		EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5; exit}' %s", S, PH_DATA)))

		# Get rigid drag coefficient
		CD = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, CD_DATA)))

		# Calculate model error
		splot FU_DATA u i:(Ca(column(i))):(R(column(i),column(i+1))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.3f,%%.3f\\n\", $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, FOL_RESULTS, TMP, TMP, FOL_RESULTS))


###############################################################################
# DEFOLIATED
###############################################################################

# Calculate values
set table DEFOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "P1" || S eq "S2") {
		# Get physical properties
		H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
		AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4; exit}' %s", S, PH_DATA)))
		V = real(system(sprintf("awk -F, '$1 == \"%s\" {print $7; exit}' %s", S, PH_DATA)))
		EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5; exit}' %s", S, PH_DATA)))

		# Get rigid drag coefficient
		CD = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3; exit}' %s", S, CD_DATA)))

		# Calculate model error
		splot FU_DATA u i:(Ca(column(i))):(R(column(i),column(i+2))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.3f,%%.3f\\n\", $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, DEFOL_RESULTS, TMP, TMP, DEFOL_RESULTS))
