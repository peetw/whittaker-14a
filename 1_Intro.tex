%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It is generally accepted that global climate change is set to increase the frequency of extreme weather events, such as heat waves, storms, droughts, and more intense flooding \citep{Wetherald-02, UKCIP-09, IPCC-13}. With respect to flooding, and fluvial flooding in particular, there is growing recognition that `hard' engineering defences are unsustainable and that a more holistic, whole-catchment approach is required in order to effectively manage flood risk \citep{ICE-01, King-04, Pitt-08}.

Within this framework, riparian vegetation and woodland can play an important role in mitigating fluvial flood risk by providing additional roughness and blockage to flood flows, reducing run-off via increased infiltration rates \citep{Marshall-14}, and encouraging large woody debris dams to form \citep{Nisbet-11}.

Numerical models are widely employed to estimate the hydraulic impact of riparian vegetation and woodland \citep{Fischer-Antze-01, Stoesser-03, Wilson-06b}. Within such models, the total hydraulic resistance of vegetation in terms of the Darcy-Weisbach friction factor $f$ can be decomposed into a bed friction factor $f'$ and a form factor $f''$, according to the linear superposition principle $f = f' + f''$ \citep{Petryk-75, Yen-02}.

Investigations into the parameterization of the form factor have typically focused on rigid and cylindrical roughness elements \citep{Li-73, James-04, Huthoff-07}. In such cases, the form factor is related to the spatially-averaged drag force $\langle F \rangle$ via:
%
\begin{equation}
f'' = \frac{8 \langle F \rangle}{s_x s_y \rho U^2}
\label{eq:form-factor}
\end{equation}
%
where $s_x$ and $s_y$ are the longitudinal and lateral spacing of the roughness elements; $\rho$ is the density of the fluid; and $U$ is the reference velocity, typically taken as the mean velocity. The classical drag force equation for a single roughness element can be expressed as:
%
\begin{equation}
F = \frac{1}{2} \rho C_d A_p U^{2}
\label{eq:classical-drag}
\end{equation}
%
where $C_d$ is the dimensionless drag coefficient and $A_p$ is the frontal projected area of the body.

For rigid roughness elements at a given level of submergence, the projected area is constant with velocity by definition and the variation in drag coefficient with Reynolds number $Re$ for cylinders and other standard objects is well documented \citep{Douglas-05}. For flexible vegetation, however, the drag force becomes much harder to predict as both the projected area and drag coefficient are a function of the flow velocity \citep{Gosselin-11, Vastila-11, Dittrich-12, Jalonen-13a}. This is due to the tendency of flexible vegetation to reconfigure and streamline in response to aero- or hydro-dynamic loading \citep{Vogel-84}, thus reducing the magnitude of the drag force they experience \citep{Harder-04, Nikora-10}.

Indeed, many atmospheric and hydraulic studies involving flexible vegetation have observed a deviation away from the quadratic force-velocity relationship described by Eq.~\eqref{eq:classical-drag} \citep{Fathi-97, Oplatka-98, Jarvela-04a, Sand-Jensen-08a, Wilson-08, Schoneboom-11, Jalonen-13b, Siniscalchi-13}. The extent of this deviation has often been expressed in terms of a Vogel exponent $\psi$ \citep{Vogel-84, Gaylord-94, Gosselin-10b, Langre-12}. In general terms and not considering the dependency of $C_d$ on $Re$, the Vogel exponent modifies the power to which the velocity is raised in the classical drag formula, so that:
%
\begin{equation}
F \propto U^{2+\psi}
\label{eq:vogel-exponent}
\end{equation}
%
where $\psi = 0$ would be valid for a rigid roughness element and $\psi = -1$ would indicate a linear force-velocity relationship, as suggested for flexible vegetation at certain velocity ranges \citep{Oplatka-98, Sand-Jensen-03, Cullen-05, Xavier-09, Folkard-11}.

It can be seen from Eqs.~\eqref{eq:classical-drag} and \eqref{eq:vogel-exponent} that the value of the Vogel exponent is directly related to the rate at which the projected area and drag coefficient decrease with increasing flow velocity, i.e. the rate of reconfiguration. As a result, the correct parameterization of the dependence of $A_p$ and $C_d$ on the flow velocity remains one of the most important problems to solve for the accurate modelling of flexible vegetation.

Although some progress has been made by combining $A_p$ and $C_d$ into a lumped `characteristic' drag coefficient $C_d A_p$ \citep{Wu-99, Armanini-05, Wilson-08}, this approach does not address the underlying issue that $A_p$ and $C_d$ are dependent on the vegetation's morphology \citep{Mayhead-73, Vogel-89, Boller-06, Dittrich-12}, biomechanical properties \citep{Fathi-97, Chen-11, Albayrak-12, Luhar-13, Whittaker-13}, and level of foliation \citep{Jarvela-02b, Armanini-05, Wilson-08, Vastila-13}.

A number of recent studies have sought to characterize $A_p$ and $C_d$ in terms of the leaf area index (LAI) and a number of species-specific parameters \citep{Dittrich-12, Jalonen-13a, Vastila-14}. The idea was first proposed by \cite{Jarvela-04a}, who determined the form factor $f''$ for just-submerged vegetation as:
%
\begin{equation}
f'' = 4 C_{d\chi} LAI \left(\frac{U}{U_\chi}\right)^\chi
\label{eq:form-factor-chi}
\end{equation}
%
where $C_{d\chi}$ is a species-specific drag coefficient; $\chi$ is also unique to a particular species and accounts for the reconfiguration of flexible vegetation; and $U_\chi$ is a scaling value included to ensure dimensional homogeneity, equal to the lowest velocity used in determining $\chi$. It can be found from rearranging Eqs.~\eqref{eq:form-factor} and \eqref{eq:form-factor-chi} that the $\chi$ value is equivalent to the Vogel exponent, i.e. $F \propto U^{2+\chi}$ \citep{Aberle-13}.

Eq.~\eqref{eq:form-factor-chi} can also be extended via linear superposition to describe the friction factors of the stem and foliage independently so that the flow resistance can be determined for vegetation with varying levels of foliation \citep{Vastila-14}. However, this then doubles the amount of parameters that are required, since separate values of $C_{d\chi}$, $U_\chi$, and $\chi$ are needed for the stem and foliage.

Although the studies listed above have shown that Eq.~\eqref{eq:form-factor-chi} may be a useful model for predicting the flow resistance of flexible vegetation, it relies on an empirical scaling factor $U_\chi$ that must be determined experimentally and is not based on physical reasoning. In addition, Eq.~\eqref{eq:form-factor-chi} is asymptotic in nature, thus potentially introducing large errors in the predicted friction factor at velocities close to zero.

To address these issues, \cite{Whittaker-13} presented a drag force model that accounts for the reconfiguration of flexible vegetation through a vegetative Cauchy number:
%
\begin{equation}
F = \frac{1}{2} \rho K \left(\frac{\rho U^2 V H}{EI}\right)^{\psi/2} U^2
\label{eq:cauchy-drag-old}
\end{equation}
%
where $K$ corresponds to an initial, combined $C_d A_p$ value; $V$, $H$, and $EI$ are the vegetation's volume, height and flexural rigidity, respectively; and the terms within the parentheses represent the vegetative Cauchy number $Ca$.

This approach allows the model to scale with varying plant size and foliation level through the combined initial projected area and drag coefficient terms within the parameter $K$, while the effects of flexible reconfiguration are explicitly incorporated through the vegetative Cauchy number. However, while the model predicted the observed drag force with reasonable accuracy for full-scale submerged trees in both foliated and defoliated states, it relies on empirical relationships for the coefficient $K$. Given that $C_d$ and $A_p$ are mostly independent, it is likely that these empirical relationships would be specific to the vegetation and flow conditions for which they were determined and thus of limited use in general modelling scenarios.

It is also unclear whether the parameterization of the Cauchy number in Eq.~\eqref{eq:cauchy-drag-old} is correct, i.e. whether $Ca$ approaches unity at the point where the vegetation begins to reconfigure. Additionally, measuring the volume of trees and other vegetation typically requires destructive methods that cannot be performed \textit{in situ} and as such may not be practical in all studies.

Therefore, this paper seeks to further improve Eq.~\eqref{eq:cauchy-drag-old} by replacing the lumped $K$ coefficient with its constituent parts, namely $C_d$ and $A_p$, so that the dependence on empirical relationships is removed. This is made possible by the recent availability of projected area data for the trees used in the study of \cite{Whittaker-13}. The Cauchy number is also redefined based on physical reasoning in order to better reflect the forces acting on the vegetation. The revised model will then be applied to high-resolution force-velocity measurements from full-scale trees, before being validated against an independent branch-scale data set.
