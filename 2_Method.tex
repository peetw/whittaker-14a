%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data and Methodology}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The drag force and physical property data sets analysed in this paper are sourced from two previous experimental studies. The larger of the two data sets was collected at the Canal de Experiencias Hidrodinamicas de El Pardo, Madrid, within the framework of the EU Hydralab III scheme. The second data set was obtained from experiments undertaken at the Leichtwei\ss-Institut f\"{u}r Wasserbau (LWI), Technische Universit\"{a}t Braunschweig. The corresponding data sets are described below.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Hydralab Experiments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this study, full-scale riparian trees were sampled from a local floodplain site and towed under fully-submerged conditions in a ship towing tank. The trees were attached upside down to a high-resolution dynamometer, which measured the forces and moments in the three Cartesian axes at a rate of 10~Hz and with an accuracy of 0.0098~N. The dynamometer itself was mounted on the underside of a towing carriage, the speed of which could be adjusted with a tolerance of 0.001~\mps{} (see \citep{Wilson-10} and \citep{Xavier-10} for full details of the experimental setup).

In total, twenty-one full-scale trees were tested, including five \textit{Alnus glutinosa} (common alder; A1--A5), four \textit{Populus nigra} (black poplar; P1--P4), and twelve \textit{Salix alba} (white willow; S1--S12) specimens. The trees were towed in both a foliated and defoliated state, with towing velocities ranging from 0.125~\mps{} to 6~\mps{}. Although flood flows rarely reach such high velocities in practice, the large range provides important test cases for any proposed models. Physical properties, such as height, stem and leaf mass, stem diameter, and stem flexural rigidity were also recorded for each of the trees \citep{Whittaker-13}.

In addition to the measured physical properties, the trees were individually photographed using a high-resolution digital camera (Canon EOS 400D). A plastic ball of known diameter was also included in each picture to provide a scale reference. Image processing software (GIMP) was then used to isolate the trees from the background based on colour variation and edge detection algorithms. This thus enabled the trees' projected areas in still air $A_{p0}$ to be determined \citep{Whittaker-14}.

It should be noted that most trees were photographed in a foliated state, with only the P1 and S2 specimens photographed in a defoliated state. Defoliated projected areas were also able to be estimated for the A1 and S1 specimens, however, as the total one-sided stem areas were recorded using a digital scanner for these and the P1 specimen. The empirical scaling factor between one-sided stem area and defoliated projected area for the P1 specimen was then applied to the one-sided stem area for A1 and S1 specimens. The resulting projected areas in still air are presented in Table~\ref{tab:specimen-properties} for each of the trees, along with their heights and main-stem flexural rigidities.

\begin{table}
\caption{Measured physical properties of the \alnus{} (A), \populus{} (P) and \salix{} (S) Hydralab trees. A dash (-) indicates data not recorded. The suffix `B' denotes a branch cut from the specimen with the same prefix.}
\centering
\sisetup{table-format=1.2}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1.3]S[table-format=1.3]S[table-format=3]}
\toprule
 & {Height} & {Foliated} & {Defoliated} & {Flex. rigid.} \\
{Specimen} & {$H$ (m)} & {$A_{p0}$ (m$^2$)} & {$A_{p0}$ (m$^2$)} & {$EI$ (\bstiff{})} \\ \midrule
A1 & 2.45 & 0.538 & 0.187~\textsuperscript{a} & 71 \\
A2 & 3.6 & 0.723 & {-} & 430 \\
A3 & 2.6 & 0.399 & {-} & 64 \\
A4 & 2.4 & 0.414 & {-} & 106 \\
A5 & 1.8 & 0.346 & {-} & 31 \\ \addlinespace
P1 & 2.68 & {-} & 0.157 & 135 \\
P2 & 3.77 & 0.701 & {-} & 122 \\
P2B1 & 3.77 & {-} & {-} & 122 \\
P2B2 & 2.5 & {-} & {-} & 37 \\
P3 & 2.6 & 0.325 & {-} & 59 \\
P4 & 3.9 & 0.684 & {-} & 664 \\
P4B1 & 2.3 & {-} & {-} & 136 \\
P4B2 & 1.8 & {-} & {-} & {-} \\ \addlinespace
S1 & 2.1 & 0.522 & 0.461~\textsuperscript{a} & {-} \\
S2 & 2.4 & {-} & 0.221 & 119 \\
S3 & 3.95 & 0.978 & {-} & 367 \\
S4 & 2 & 0.317 & {-} & 31 \\
S5 & 3.6 & 1.153 & {-} & {-} \\
S5B1 & 3.6 & {-} & {-} & 65 \\
S5B2 & 3.6 & {-} & {-} & 157 \\
S6 & 3.2 & 0.734 & {-} & {-} \\
S6B1 & 3.2 & {-} & {-} & 20 \\
S6B2 & 2.8 & {-} & {-} & 47 \\
S7 & 2.3 & 0.52 & {-} & {-} \\
S7B1 & 2.3 & {-} & {-} & 85 \\
S7B2 & 2.3 & {-} & {-} & 84 \\
S7B3 & 2.18 & {-} & {-} & {-} \\
S8 & 3 & 0.363 & {-} & 35 \\
S9 & 3.6 & 0.601 & {-} & {-} \\
S10 & 3.24 & 0.654 & {-} & 258 \\
S11 & 3.5 & 0.343 & {-} & 64 \\
S12 & 4.1 & 0.327 & {-} & 141 \\ \bottomrule \addlinespace
\multicolumn{5}{l}{\textsuperscript{a} Projected area estimated from total one-sided stem area}
\end{tabular}
\end{footnotesize}
\label{tab:specimen-properties}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{LWI Experiments}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:LWI}

The data from this study consist of drag force measurements for isolated branches of natural willow, natural poplar, and artificial poplar, tested in partially ($h = 0.12$~m) and fully ($h = 0.25$~m) submerged conditions and with flow velocities ranging from 0.198~\mps{} to 0.957~\mps{}. The data were obtained under steady uniform flow conditions in a tilting flume of length 32~m, width 0.6~m and depth 0.4~m.

Drag forces were recorded using a custom strain gauge setup (described in detail in \citep{Schoneboom-08}) that had a temporal resolution of 200~Hz and an accuracy of 0.02~N. Full details of the branches and experimental procedure can be found in \cite{Schoneboom-11}. The artificial poplars are included here as they have been found to show similar resistance behaviour to their natural counterparts \citep{Dittrich-12}.

In addition to the force-velocity data, key physical properties, such as the stem diameter, height, and projected area in still air were recorded. Bending stiffness tests were also carried out to determine the elastic modulus for the wire stem of the artificial poplar \citep{Schoneboom-11}. For the two natural branches, main-stem elastic moduli are taken from the species-averaged values for the Hydralab trees \citep{Whittaker-13}. The measured physical properties are summarized in Table~\ref{tab:LWI-specimens}. Note that the water depth is used as the specimen height for the partially submerged cases.

\begin{table}
\caption{Measured physical properties of the poplar (P) and willow (W) LWI branches. The suffixes `A' and `N' denote artificial and natural branches, respectively, while `1' and `2' denote partially and fully submerged flow conditions, respectively.}
\centering
\sisetup{table-format=2}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=3]S[table-format=1.1]S[table-format=1.2e2]}
\toprule
 & {Height} & {Foliated} & {Stem diam.} & {Elastic mod.} \\
{Specimen} & {$H$ (cm)} & {$A_{p0}$ (cm$^2$)} & {$d$ (mm)} & {$E$ (\ymod{})} \\ \midrule
WN1 & 12 & 77 & 3.2 & 4.55e9~\textsuperscript{a} \\
WN2 & 25 & 205 & 3.2 & 4.55e9~\textsuperscript{a} \\
PN1 & 12 & 137 & 3.6 & 5.94e9~\textsuperscript{a} \\
PN2 & 24 & 154 & 3.6 & 5.94e9~\textsuperscript{a} \\
PA1 & 12 & 95 & 3 & 6.97e9 \\
PA2 & 23 & 148 & 3 & 6.97e9 \\ \bottomrule \addlinespace
\multicolumn{5}{l}{\textsuperscript{a} Elastic modulus taken from Hydralab trees' species-average}
\end{tabular}
\end{footnotesize}
\label{tab:LWI-specimens}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Model Theory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

When investigating the drag force of flexible vegetation, a number of previous studies have found that the complex system of flow-body interactions can be characterized using a collection of dimensionless numbers; namely, the drag coefficient $C_d$, Reynolds number $Re$, buoyancy $B$, reconfiguration number $\mathcal{R}$, and Cauchy number $Ca$ \citep{Alben-02, Langre-08, Luhar-11, Whittaker-13}. The drag coefficient and Reynolds number are well-known and hence not redefined here. Additionally, buoyancy forces are assumed to be negligible for the plant types examined herein.

For regions where the drag coefficient is independent of the Reynolds number, it has been shown that the reconfiguration of flexible objects, such as plates and fibres, is a function of the Cauchy number \citep{Gosselin-10b, Gosselin-11, Barois-13}. The effect of reconfiguration is expressed in terms of a dimensionless number $\mathcal{R}$, which compares the measured drag force to that of an equivalent rigid object with the same morphology:
%
\begin{equation}
\mathcal{R} = \frac{2F}{\rho C_{d0} A_{p0} U^2}
\label{eq:reconfiguration}
\end{equation}
%
where $C_{d0}$ is the `rigid' drag coefficient; and $A_{p0}$ is the projected area in still air.

The Cauchy number quantifies the compressibility of objects in a fluid flow through the ratio of the inertial and elastic forces \citep{Blevins-90, Cermak-98, Chakrabarti-02}. In this study, the dimensionless Cauchy number is parameterized as:
%
\begin{equation}
Ca = \frac{\rho U^2 A_{p0} H^2}{EI}
\label{eq:cauchy-number}
\end{equation}
%
where $EI$ is the flexural rigidity of the vegetation's main stem; and $H$ is the vegetation's undeflected height.

\cite{Luhar-11} predicted the drag force and posture of idealized seagrasses and marine macroalgae with high accuracy using a similar Cauchy number to Eq.~\eqref{eq:cauchy-number}. In this study, however, the product of the vegetation's breadth and length ($b l^3$) \citep{Luhar-11} is replaced by $A_{p0} H^2$ since this more accurately describes the flow-interacting area when considering objects with complex morphology, such as full-scale trees. It should be noted that in the case of idealized slender beams, $A_{p0} H^2$ and $b l^3$ are equivalent.

Assuming that the density of the fluid is constant, the deviation of the reconfiguration number $\mathcal{R}$ away from unity with increasing velocity for flexible vegetation must be caused solely by the change in the characteristic drag coefficient term in Eq.~\eqref{eq:classical-drag}. Considering that $\mathcal{R} = f(Ca)$ \citep{Gosselin-10b, Gosselin-11, Langre-12, Barois-13}, it follows that the characteristic drag coefficient is a function of the Cauchy number, i.e. $C_d A_p = f(Ca)$ \citep{Langre-08}. Furthermore, assuming that the relationship between $\mathcal{R}$ and $Ca$ follows a power law, it can be found that the power to which the Cauchy number is raised is equivalent to half the Vogel exponent ($\psi / 2$). The variation in the characteristic drag coefficient with velocity can therefore be described by the following:
%
\begin{equation}
C_d A_p = C_{d0} A_{p0} Ca^{\psi/2}
\label{eq:cauchy-CdAp}
\end{equation}
%
where $C_{d0}$ is taken to be species-specific, similarly to $C_{d\chi}$ in Eq.~\eqref{eq:form-factor-chi}. The Vogel exponent is also thought to be species-specific \citep{Jarvela-04a, Aberle-12, Aberle-13}.

Physically, Eq.~\eqref{eq:cauchy-CdAp} represents the magnitude ($Ca$) and rate ($\psi$) of the reconfiguration that flexible objects experience in fluid flows, through the reduction in the characteristic drag coefficient from its initial `rigid' value ($C_{d0} A_{p0}$) with increasing velocity. It is also applicable to rigid bodies, since when $\psi = 0$ there will be no variation in the characteristic drag coefficient, assuming independence of Reynolds number effects.

It should be noted that, since a Cauchy number of less than unity indicates that there is no deformation or reconfiguration by definition \citep{Blevins-90, Cermak-98, Chakrabarti-02}, a limiter must be placed on the Cauchy number in Eq.~\eqref{eq:cauchy-CdAp}, i.e. $Ca = \max(1, Ca)$. This prevents the characteristic drag coefficient deviating away from its initial rigid value of $C_{d0} A_{p0}$ when the drag force exerted on an object is not great enough to overcome the object's bending resistance and cause reconfiguration.

Replacing the characteristic drag coefficient in Eq.~\eqref{eq:classical-drag} with the relationship defined in Eq.~\eqref{eq:cauchy-CdAp}, we thus arrive at the new `Cauchy model' for predicting the hydrodynamic drag force exerted on flexible vegetation:
%
\begin{equation}
F = \frac{1}{2} \rho C_{d0} A_{p0} Ca^{\psi/2} U^2
\label{eq:cauchy-drag}
\end{equation}

One of the main advantages of this model compared to the one described by Eq.~\eqref{eq:cauchy-drag-old} is that the lumped empirical coefficient $K$ has been divided into its more physically correct constituent terms, namely the rigid drag coefficient and initial projected area. This removes the dependence on empirical relationships for $K$ and allows the model to be more general in application. In addition, the parameterization of the Cauchy number is now more physically correct and no longer requires the vegetation's volume, which is typically impractical to measure \textit{in situ}.
