#!/bin/sh

viewHelp () {
cat << EOF
# -----------------------------------------------------------------------------
# Use git-latexdiff to create a pdf highlighting changes from previous version
#
#   Usage:
#       $0 [options]
#
#   Options:
#       -o <SHA>  SHA key of old commit (default: previous commit)
#       -n <SHA>  SHA key of new commit (default: current HEAD)
#       -h        Display this text
# -----------------------------------------------------------------------------
EOF
[ "$1" ] && echo "\n  *** $1 ***\n"
exit 1
}

# Default values
OLD="HEAD~1"
NEW="HEAD"
INPUT="$(basename $(pwd -P)).tex"
OUTPUT="changes.pdf"

# Process command line options
while getopts ":o:n:h" opt; do
	case $opt in
		o)
			OLD=$OPTARG
			;;
		n)
			NEW=$OPTARG
			;;
		h)
			viewHelp
			;;
		\?)
			viewHelp "Invalid option: -$OPTARG"
			;;
		:)
			viewHelp "Option -$OPTARG requires an argument"
			;;
	esac
done

# Run latexdiff using the two commit snapshots
git-latexdiff --verbose --bibtex --mode errorstopmode --main "$INPUT" --output "$OUTPUT" $OLD $NEW

# Reset git index
git reset --mixed HEAD > /dev/null
