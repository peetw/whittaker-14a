%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and Discussion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The drag force model proposed here (Eq.~\ref{eq:cauchy-drag}) depends on a number of the vegetation's physical properties, including the undeflected height; projected area in still air; the diameter and elastic modulus of the main stem; and the species-specific drag coefficient and Vogel exponent. With the exception of the elastic modulus, for which values may be readily taken from existing literature in lieu of direct measurements \citep{McBurney-62, Winandy-94, FPL-10, Shmulsky-11}, only the species-specific drag coefficient and Vogel exponent cannot be determined from a simple site survey.

It is hoped that eventually sufficient data will have been collected that these may also be taken from the literature. At present, however, drag force data for full-scale trees and other flexible vegetation is scarce. Therefore, before applying the model to the two data sets, species-specific values for $C_{d0}$ and $\psi$ are determined using the experimental data from the Hydralab study.

The availability of the trees' projected areas in still air (Table~\ref{tab:specimen-properties}) enabled initial `rigid' drag coefficients to be calculated for the lowest towing velocities where the trees were observed to undergo minimal reconfiguration, i.e.:
%
\begin{equation}
C_{d0} = \frac{2 F_0}{\rho A_{p0} U^2_0}
\label{eq:rigid-Cd}
\end{equation}
%
where $F_0$ and $U_0$ denote the force-velocity pair obtained at the lowest towing velocity. For all specimens this velocity was 0.125~\mps{}. The resulting species-averaged rigid drag coefficients for the Hydralab trees are presented in Table~\ref{tab:model-params}.

\begin{table}[htb]
\caption{Species-averaged rigid drag coefficients and Vogel exponents for the foliated and defoliated Hydralab trees. The number of specimens $N$ considered for each parameter is also given.}
\centering
\sisetup{table-format=2}
\begin{footnotesize}
\begin{tabular}{lS[table-format=.2]SS[table-format=-.2]SS[table-format=.2]SS[table-format=-.2]S}
\toprule
 & \multicolumn{4}{c}{Foliated} & \multicolumn{4}{c}{Defoliated} \\ \cmidrule(lr){2-5} \cmidrule(lr){6-9}
{Species} & {$C_{d0}$} & {$N$} & {$\psi$} & {$N$} & {$C_{d0}$} & {$N$} & {$\psi$} & {$N$} \\ \midrule
\textit{A. glutinosa} & 0.99 & 5 & -0.73 & 5 & 0.79 & 1 & -0.57 & 4 \\
\textit{P. nigra} & 0.76 & 3 & -0.8 & 7 & 1.04 & 1 & -0.81 & 6 \\
\textit{S. alba} & 0.88 & 11 & -0.81 & 19 & 0.94 & 2 & -0.84 & 16 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:model-params}
\end{table}

From Table~\ref{tab:model-params} it can be seen that the drag coefficients are generally higher when the trees are defoliated. This is thought to be due to the fact that the defoliated trees more closely resemble a collection of rigid cylinders at low velocities and thus have a drag coefficient of around unity \citep{Shames-03}, while the foliage on the foliated trees is more flexible and thus lowers the overall drag coefficient as it streamlines, even at the low velocities considered for Eq.~\eqref{eq:rigid-Cd}.

The exception to this pattern are the alder trees, although only one defoliated $C_{d0}$ value was available and this was calculated using a projected area in still air that was estimated from the tree's total one-sided stem area rather than determined directly from photographic analysis (see specimen A1 in Table~\ref{tab:specimen-properties}).

It is also interesting to note that the rigid drag coefficients calculated here are considerably greater than those found for similar species via the application of Eq.~\eqref{eq:form-factor-chi}. For example, \cite{Aberle-13} summarized $C_{d\chi}$ values from a number of previous studies, which included specimens of \textit{Populus nigra} ($C_{d\chi} = 0.33$), \textit{Salix caprea} ($C_{d\chi} = 0.43$) and \textit{Salix triandra x viminalis} ($C_{d\chi} = 0.53$).

The lower values for $C_{d\chi}$ calculated in those studies are most likely a result of the fact that the full velocity range is used to derive $C_{d\chi}$ from Eq.~\eqref{eq:form-factor-chi}, whereas only the initial velocity is required in Eq.~\eqref{eq:rigid-Cd}. As a consequence, $C_{d\chi}$ may be more difficult to compare across experimental studies as it is dependent on both the lower and upper velocities achieved during experimental testing, whereas $C_{d0}$ is only dependent on the lower velocity.

The calculation of initial, rigid drag coefficients also allows the parameterization of the Cauchy number to be verified, since the specimens' reconfiguration numbers can now be determined from Eq.~\eqref{eq:reconfiguration}. The variation in reconfiguration number with Cauchy number (Eq.~\ref{eq:cauchy-number}) for the foliated and defoliated trees is plot in Fig.~\ref{fig:fig-1-cauchy-reconf}. For comparison, the variation in reconfiguration number with Cauchy numbers determined using the previous approach (Eq.~\ref{eq:cauchy-drag-old}) is also included \citep{Whittaker-13}.

\begin{figure}[htb]
\centering
\includegraphics{fig-1-cauchy-reconf}
\caption{Variation in reconfiguration number with the Cauchy number for the: (a) foliated; and (b) defoliated Hydralab trees.}
\label{fig:fig-1-cauchy-reconf}
\end{figure}

From Fig.~\ref{fig:fig-1-cauchy-reconf}, it can be seen that there is a considerable difference in the magnitude of the Cauchy number depending on whether the previous (Eq.~\ref{eq:cauchy-drag-old}) or current (Eq.~\ref{eq:cauchy-number}) parameterization is used. When utilizing the previous approach, the Cauchy number remains below unity despite the tree's undergoing considerable reconfiguration. On-the-other-hand, the Cauchy number presented in this study results in the expected behaviour, i.e. that the drag force on the specimens only deviates away from the rigid approximation once the trees begin to reconfigure ($Ca > 1$). This thus suggests that the current parameterization of the vegetative Cauchy number in Eq.~\eqref{eq:cauchy-number} is valid for both the foliated and defoliated states.

In the previous section, it was assumed that the relationship between the reconfiguration number and the Cauchy number follows a power law, with that power being equivalent to $\psi / 2$. For the current Cauchy number (Eq.~\ref{eq:cauchy-number}), it can be seen from Fig.~\ref{fig:fig-1-cauchy-reconf} that the relationship between $\mathcal{R}$ and $Ca$ may indeed be approximated by a power law in the region where the trees are reconfiguring ($Ca > 1$).

Although the Vogel exponent $\psi$ was originally formulated to investigate regions of high deformation \citep{Vogel-84}, it has since been determined from the full force-velocity range in many studies involving flexible vegetation \citep{Aberle-12, Jalonen-13a, Vastila-13}. This is mainly due to the subjectivity of specifying lower or upper cut-off velocities. In this study, therefore, Vogel exponents are calculated from a power-law regression analysis using the full force-velocity range ($R^2 > 0.97$). The resulting species-averaged values are summarized in Table~\ref{tab:model-params}. Discussion and comparison of the Vogel exponent values to those in the literature can be found in \cite{Whittaker-13}.

Utilizing the trees' physical properties from Table~\ref{tab:specimen-properties} and the species-averaged drag coefficients and Vogel exponents from Table~\ref{tab:model-params}, the Cauchy model according to Eq.~\eqref{eq:cauchy-drag} can now be employed to predict the drag force for each of the Hydralab trees. The resulting drag forces are plot against the measured drag forces in Fig.~\ref{fig:fig-2-cauchy-pred}, for each of the foliated and defoliated trees.

\begin{figure}[htb]
\centering
\includegraphics{fig-2-cauchy-pred}
\caption{Drag force as predicted using the Cauchy model for the: (a) foliated; and (b) defoliated Hydralab trees.}
\label{fig:fig-2-cauchy-pred}
\end{figure}

The close agreement between the predicted and measured drag forces is evident in Fig.~\ref{fig:fig-2-cauchy-pred}, for both the foliated and defoliated trees. Given the large sample sizes for the foliated specimens, this thus shows that using species-specific values for both the rigid drag coefficients and the Vogel exponents is a valid approach. It is important to note that, apart from the measurable physical properties in Table~\ref{tab:specimen-properties}, these are the only additional parameters that are required in the current model.

The accuracy of the model's predictions is quantified in Table~\ref{tab:cauchy-error}, which presents the average error for each of the species. It also includes the average errors for the previous Cauchy model (Eq.~\ref{eq:cauchy-drag-old}) and a rigid drag force model where the effects of reconfiguration are ignored, i.e. $\psi = 0$. The comparison with the rigid model is useful as rigid approximations are often used to represent vegetation in numerical modelling studies \citep{Stoesser-03, Gao-11, Zhang-13}. When using the previous Cauchy model \citep{Whittaker-13}, it was not possible to determine $K$--$V$ relationships, and thus average errors, for the poplar trees as only one poplar specimen had its volume recorded.

\begin{table}[htb]
\caption{Average percentage errors $\epsilon_{\%}$ for the Cauchy and rigid drag force models' predictions for the foliated and defoliated Hydralab trees. The number of data points $N$ used to calculate the average errors is also given.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=2]SS[table-format=2]S[table-format=3.1]S[table-format=3]}
\toprule
 & \multicolumn{2}{c}{Current} & \multicolumn{2}{c}{Previous \citep{Whittaker-13}} & \multicolumn{2}{c}{Rigid} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7}
{Species} & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} \\ \midrule
\textit{A. glutinosa} (fol.) & 17.4 & 39 & 36.6 & 33 & 195.9 & 39 \\
\textit{A. glutinosa} (defol.) & 10.1 & 10 & 43 & 28 & 115 & 10 \\
\textit{P. nigra} (fol.) & 22.9 & 28 & {-} & {-} & 224.5 & 28 \\
\textit{P. nigra} (defol.) & 17.6 & 8 & {-} & {-} & 82.8 & 8 \\
\textit{S. alba} (fol.) & 24.3 & 54 & 47.5 & 88 & 255.9 & 101 \\
\textit{S. alba} (defol.) & 32.9 & 11 & 42.7 & 136 & 220.7 & 23 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:cauchy-error}
\end{table}

It can be seen from Table~\ref{tab:cauchy-error} that the Cauchy model proposed here (Eq.~\ref{eq:cauchy-drag}) represents an order of magnitude improvement over a rigid approximation, thus highlighting the unsuitability of such approximations when modelling flexible vegetation. The new model is also considerably more accurate than the one previously defined by \cite{Whittaker-13} (Eq.~\ref{eq:cauchy-drag-old}). This is probably largely due to the improvement in the parameterization of the Cauchy number (see Fig.~\ref{fig:fig-1-cauchy-reconf}).

The predictions for the defoliated trees appear to be more accurate than those for the foliated trees. This is most likely due to the small sample sizes used to determine the species-specific values for the defoliated trees and thus the species-averaged values better reflect the individual specimens. However, for both the foliated and defoliated trees, the good agreement between the measured and predicted drag forces suggests that the current approach, namely using a vegetative Cauchy number and two species-specific parameters, is useful.

In order to validate the Cauchy approach, the model is applied to the independent force-velocity data for the willow and poplar branches from the LWI experiments. The physical properties for the branches are taken from Table~\ref{tab:LWI-specimens}, while the species-averaged rigid drag coefficients and Vogel exponents are taken to be the same as those determined for the foliated willow and poplar Hydralab trees (Table~\ref{tab:model-params}). The resulting drag force predictions and average errors are presented in Fig.~\ref{fig:fig-3-LWI-cauchy-pred} and Table~\ref{tab:LWI-cauchy-error}, respectively. Average errors for drag forces obtained through a rigid approximation (i.e. $\psi = 0$) are also included for reference in Table~\ref{tab:LWI-cauchy-error}. Unfortunately, it was not possible to calculate average errors for the previous Cauchy model (Eq.~\ref{eq:cauchy-drag-old}) since the branches' volumes were not recorded.

\begin{figure}[htb]
\centering
\includegraphics{fig-3-LWI-cauchy-pred}
\caption{Drag force as predicted using the Cauchy model for the: (a) partially submerged; and (b) fully submerged LWI branches.}
\label{fig:fig-3-LWI-cauchy-pred}
\end{figure}

From Fig.~\ref{fig:fig-3-LWI-cauchy-pred} it can be seen that there is excellent agreement between the Cauchy model and the measured drag forces for the fully submerged branches. Indeed, the average errors for the Cauchy model in this case are in the range of \SIrange{6.8}{12}{\percent}, which represents a substantial increase in accuracy over the rigid approximation ($\epsilon = $\SIrange{116}{255}{\percent}).

\begin{table}[htb]
\caption{Average percentage errors $\epsilon_{\%}$ for the Cauchy and rigid drag force models' predictions for the partially and fully submerged LWI branches.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=3]SS[table-format=3]}
\toprule
 & \multicolumn{2}{c}{Partially sub.} & \multicolumn{2}{c}{Fully sub.} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
{Specimen} & {Cauchy $\epsilon_{\%}$} & {Rigid $\epsilon_{\%}$} & {Cauchy $\epsilon_{\%}$} & {Rigid $\epsilon_{\%}$} \\ \midrule
WN & 20 & 152 & 12 & 255 \\
PN & 17.9 & 101 & 8.5 & 116 \\
PA & 18 & 117 & 6.8 & 142 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:LWI-cauchy-error}
\end{table}

The model appears to be somewhat less accurate when applied to the partially submerged branches, where the average errors range from \SIrange{17.9}{20}{\percent}. Although the gradient of the force-velocity curve is well-captured for the partially submerged condition, the over-prediction of the drag force by both the Cauchy and rigid models suggests that the initial, rigid drag coefficients may have been slightly over-estimated. This is most likely a result of the fact that the drag coefficients were taken from the Hydralab data where the specimens were fully submerged.

It is expected that the inability of the flow to circulate over the top of the branches in the partially submerged case would alter the pressure distribution and thus the drag coefficients \citep{Wu-99, Alonso-04}. However, despite using the fully submerged drag coefficients, the Cauchy model still represents a considerable improvement over the rigid approximation ($\epsilon = $\SIrange{101}{152}{\percent}).
